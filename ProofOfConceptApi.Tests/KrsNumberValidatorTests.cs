﻿using FluentAssertions;
using ProofOfConceptApi.Helpers.Validators;
using Xunit;

namespace ProofOfConceptApi.Tests
{
    public sealed class KrsNumberValidatorTests
    {
        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("0000706466", true)]
        [InlineData("0000133156", true)]
        [InlineData("00001331561", false)]
        [InlineData("000013315", false)]
        [InlineData("77777", false)]
        public void Validate_Validates_Krs_Number(string value, bool expected)
        {
            // Arrange
            IValidator validator = new KrsNumberValidator();

            // Act
            var result = validator.Validate(value);

            // Assert
            result.Should().Be(expected);
        }
    }
}