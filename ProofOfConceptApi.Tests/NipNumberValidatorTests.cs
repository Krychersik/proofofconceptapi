using FluentAssertions;
using ProofOfConceptApi.Helpers.Validators;
using Xunit;

namespace ProofOfConceptApi.Tests
{
    public sealed class NipNumberValidatorTests
    {
        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("PL7777777777", true)]
        [InlineData("777-777-77-77", true)]
        [InlineData("7777777777", true)]
        [InlineData("77777777778", false)]
        [InlineData("777 777 77 77", false)]
        [InlineData("PL-777-777-77-77", false)]
        [InlineData("PL77777777778", false)]
        [InlineData("PL77777777", false)]
        [InlineData("7777777777/", false)]
        [InlineData("7777ba777777/", false)]
        [InlineData("7777ba777776/", false)]
        [InlineData("77777", false)]
        [InlineData("pl77777", false)]
        [InlineData("pl7777777777", false)]
        public void Validate_Validates_Nip_Number(string value, bool expected)
        {
            // Arrange
            IValidator validator = new NipNumberValidator();

            // Act
            var result = validator.Validate(value);

            // Assert
            result.Should().Be(expected);
        }
    }
}