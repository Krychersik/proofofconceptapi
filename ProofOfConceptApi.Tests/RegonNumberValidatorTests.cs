﻿using FluentAssertions;
using ProofOfConceptApi.Helpers.Validators;
using Xunit;

namespace ProofOfConceptApi.Tests
{
    public sealed class RegonNumberValidatorTests
    {
        [Theory]
        [InlineData(null, false)]
        [InlineData("", false)]
        [InlineData("26 – 090345 - 6", false)]
        [InlineData("260903456", true)]
        [InlineData("382654240", true)]
        [InlineData("331438036", true)]
        [InlineData("2609034561", false)]
        [InlineData("260903455", false)]
        [InlineData("26 090345 6", false)]
        [InlineData("26/090345/6", false)]
        [InlineData("PL7777777777", false)]
        [InlineData("7777777777", false)]
        public void Validate_Validates_Regon_Number(string value, bool expected)
        {
            // Arrange
            IValidator validator = new RegonNumberValidator();

            // Act
            var result = validator.Validate(value);

            // Assert
            result.Should().Be(expected);
        }
    }
}