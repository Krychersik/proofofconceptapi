﻿namespace ProofOfConceptApi.Contracts.V1.Models
{
    public sealed class AddressDto
    {
        public string Street { get; set; }
        public string Number { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}