﻿namespace ProofOfConceptApi.Contracts.V1.Models
{
    public sealed class CompanyDto
    {
        public string Name { get; set; }
        public string Nip { get; set; }
        public string Krs { get; set; }
        public string Regon { get; set; }

        public AddressDto Address { get; set; }
    }
}