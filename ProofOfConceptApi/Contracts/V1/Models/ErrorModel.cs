﻿namespace ProofOfConceptApi.Contracts.V1.Models
{
    public sealed class ErrorModel
    {
        public string FieldName { get; set; }
        public string Message { get; set; }
    }
}