﻿using System.Collections.Generic;
using ProofOfConceptApi.Contracts.V1.Models;

namespace ProofOfConceptApi.Contracts.V1.Responses
{
    public sealed class ErrorResponse
    {
        public List<ErrorModel> Errors { get; set; } = new List<ErrorModel>();
    }
}