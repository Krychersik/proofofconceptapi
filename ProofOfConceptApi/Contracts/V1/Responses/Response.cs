﻿namespace ProofOfConceptApi.Contracts.V1.Responses
{
    public sealed class Response<T>
    {
        public T Data { get; set; }

        public Response(T response)
        {
            Data = response;
        }
    }
}