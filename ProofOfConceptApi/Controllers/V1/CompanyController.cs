﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProofOfConceptApi.Contracts.V1;
using ProofOfConceptApi.Contracts.V1.Models;
using ProofOfConceptApi.Contracts.V1.Responses;
using ProofOfConceptApi.Services;

namespace ProofOfConceptApi.Controllers.V1
{
    [ApiController]
    public sealed class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;
        private readonly IStatisticService _statisticService;

        public CompanyController(ICompanyService companyService, IStatisticService statisticService)
        {
            _companyService = companyService;
            _statisticService = statisticService;
        }

        [HttpGet(ApiRoutes.Company.GetByIdentifier)]
        public async Task<ActionResult<Response<CompanyDto>>> GetByIdentifier([FromQuery] string identifier)
        {
            await _statisticService.AddStatistic(identifier, Request.Headers);

            var companyDto = await _companyService.GetCompanyByIdentifierAsync(identifier);

            if (companyDto == null)
            {
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>
                    {
                        new ErrorModel
                        {
                            Message =
                                $"Company with with identifier {identifier} couldn't be found or the identifier was incorrect."
                        }
                    }
                });
            }

            var response = new Response<CompanyDto>(companyDto);

            return response;
        }
    }
}