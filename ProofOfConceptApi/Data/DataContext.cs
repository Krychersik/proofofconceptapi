﻿using Microsoft.EntityFrameworkCore;
using ProofOfConceptApi.Data.Models;

namespace ProofOfConceptApi.Data
{
    public sealed class DataContext : DbContext, IDataContext
    {
        public DataContext(DbContextOptions options) : base(options) { }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Statistic> Statistics { get; set; }
        public DbSet<Header> Headers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            BuildCompaniesModel(modelBuilder);
            BuildAddressesModel(modelBuilder);
            BuildStatisticsModel(modelBuilder);
        }

        private void BuildCompaniesModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>()
                .HasOne(company => company.Address)
                .WithOne(address => address.Company)
                .HasForeignKey<Address>(address => address.CompanyId);

            modelBuilder.Entity<Company>()
                .Property(company => company.Name)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Company>()
                .Property(company => company.Nip)
                .HasMaxLength(10);

            modelBuilder.Entity<Company>()
                .Property(company => company.Regon)
                .HasMaxLength(9);

            modelBuilder.Entity<Company>()
                .Property(company => company.Krs)
                .HasMaxLength(10);

            modelBuilder
                .Entity<Company>()
                .HasData(
                    new Company
                    {
                        Id = 1,
                        Name = "Blizzard Entertainment",
                        Nip = "5992648355",
                        Regon = "382654240",
                        Krs = "0000706466"
                    },
                    new Company
                    {
                        Id = 2,
                        Name = "id Software",
                        Nip = null,
                        Regon = "331438036",
                        Krs = null
                    },
                    new Company
                    {
                        Id = 3,
                        Name = "Dotnond Ltd",
                        Nip = null,
                        Regon = null,
                        Krs = "0000133156"
                    },
                    new Company
                    {
                        Id = 4,
                        Name = "Guerrilla Games",
                        Nip = "7820033114",
                        Regon = null,
                        Krs = null
                    },
                    new Company
                    {
                        Id = 5,
                        Name = "My Test Company",
                        Nip = "7777777777",
                        Regon = null,
                        Krs = null
                    },
                    new Company
                    {
                        Id = 6,
                        Name = "Test Company Without Address",
                        Nip = "6690001315",
                        Regon = null,
                        Krs = null
                    }
                );
        }

        private void BuildAddressesModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .Property(address => address.Street)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<Address>()
                .Property(address => address.Number)
                .HasMaxLength(10)
                .IsRequired();

            modelBuilder.Entity<Address>()
                .Property(address => address.PostalCode)
                .HasMaxLength(6)
                .IsRequired();

            modelBuilder.Entity<Address>()
                .Property(address => address.City)
                .HasMaxLength(30)
                .IsRequired();

            modelBuilder
                .Entity<Address>()
                .HasData(
                    new Address
                    {
                        Id = 1,
                        Street = "Dworcowa",
                        Number = "7",
                        PostalCode = "12-123",
                        City = "Warszawa",
                        Country = "Polska",
                        CompanyId = 1
                    },
                    new Address
                    {
                        Id = 2,
                        Street = "Akustyczna",
                        Number = "3",
                        PostalCode = "15-151",
                        City = "Gdynia",
                        Country = "Polska",
                        CompanyId = 2
                    },
                    new Address
                    {
                        Id = 3,
                        Street = "Mielżyńskiego",
                        Number = "23",
                        PostalCode = "61-523",
                        City = "Poznań",
                        Country = "Polska",
                        CompanyId = 3
                    },
                    new Address
                    {
                        Id = 4,
                        Street = "Poznańska",
                        Number = "12",
                        PostalCode = "61-928",
                        City = "Poznań",
                        Country = "Polska",
                        CompanyId = 4
                    },
                    new Address
                    {
                        Id = 5,
                        Street = "Testowa",
                        Number = "1",
                        PostalCode = "61-458",
                        City = "Poznań",
                        Country = "Polska",
                        CompanyId = 5
                    }
                );
        }

        private void BuildStatisticsModel(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Statistic>()
                .HasMany(h => h.Headers)
                .WithOne(s => s.Statistic)
                .HasForeignKey(s => s.StatisticId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}