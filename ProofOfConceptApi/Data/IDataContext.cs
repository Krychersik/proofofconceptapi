﻿using Microsoft.EntityFrameworkCore;
using ProofOfConceptApi.Data.Models;

namespace ProofOfConceptApi.Data
{
    public interface IDataContext
    {
        DbSet<Company> Companies { get; set; }
        DbSet<Address> Addresses { get; set; }
        DbSet<Statistic> Statistics { get; set; }
        DbSet<Header> Headers { get; set; }
    }
}