﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ProofOfConceptApi.Data;

namespace ProofOfConceptApi.Data.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20191109183644_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ProofOfConceptApi.Data.Models.Address", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.Property<int>("CompanyId")
                        .HasColumnType("int");

                    b.Property<string>("Number")
                        .IsRequired()
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("PostalCode")
                        .IsRequired()
                        .HasColumnType("nvarchar(6)")
                        .HasMaxLength(6);

                    b.Property<string>("Street")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId")
                        .IsUnique();

                    b.ToTable("Addresses");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            City = "Warszawa",
                            CompanyId = 1,
                            Number = "7",
                            PostalCode = "12-123",
                            Street = "Dworcowa"
                        },
                        new
                        {
                            Id = 2,
                            City = "Gdynia",
                            CompanyId = 2,
                            Number = "3",
                            PostalCode = "15-151",
                            Street = "Akustyczna"
                        },
                        new
                        {
                            Id = 3,
                            City = "Poznań",
                            CompanyId = 3,
                            Number = "23",
                            PostalCode = "61-523",
                            Street = "Mielżyńskiego"
                        },
                        new
                        {
                            Id = 4,
                            City = "Poznań",
                            CompanyId = 4,
                            Number = "12",
                            PostalCode = "61-928",
                            Street = "Poznańska"
                        },
                        new
                        {
                            Id = 5,
                            City = "Poznań",
                            CompanyId = 5,
                            Number = "1",
                            PostalCode = "61-458",
                            Street = "Testowa"
                        });
                });

            modelBuilder.Entity("ProofOfConceptApi.Data.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Krs")
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Nip")
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("Regon")
                        .HasColumnType("nvarchar(9)")
                        .HasMaxLength(9);

                    b.HasKey("Id");

                    b.ToTable("Companies");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Krs = "0000706466",
                            Name = "Blizzard Entertainment",
                            Nip = "5992648355",
                            Regon = "080248249"
                        },
                        new
                        {
                            Id = 2,
                            Name = "id Software",
                            Regon = "300002030"
                        },
                        new
                        {
                            Id = 3,
                            Krs = "0000133156",
                            Name = "Dotnond Ltd"
                        },
                        new
                        {
                            Id = 4,
                            Name = "Guerrilla Games",
                            Nip = "7820033114"
                        },
                        new
                        {
                            Id = 5,
                            Name = "My Test Company",
                            Nip = "7777777777"
                        },
                        new
                        {
                            Id = 6,
                            Name = "Test Company Without Address",
                            Nip = "6690001315"
                        });
                });

            modelBuilder.Entity("ProofOfConceptApi.Data.Models.Address", b =>
                {
                    b.HasOne("ProofOfConceptApi.Data.Models.Company", "Company")
                        .WithOne("Address")
                        .HasForeignKey("ProofOfConceptApi.Data.Models.Address", "CompanyId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
