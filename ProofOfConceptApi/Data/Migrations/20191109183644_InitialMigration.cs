﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProofOfConceptApi.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Nip = table.Column<string>(maxLength: 10, nullable: true),
                    Krs = table.Column<string>(maxLength: 10, nullable: true),
                    Regon = table.Column<string>(maxLength: 9, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Street = table.Column<string>(maxLength: 50, nullable: false),
                    Number = table.Column<string>(maxLength: 10, nullable: false),
                    PostalCode = table.Column<string>(maxLength: 6, nullable: false),
                    City = table.Column<string>(maxLength: 30, nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Companies",
                columns: new[] { "Id", "Krs", "Name", "Nip", "Regon" },
                values: new object[,]
                {
                    { 1, "0000706466", "Blizzard Entertainment", "5992648355", "080248249" },
                    { 2, null, "id Software", null, "300002030" },
                    { 3, "0000133156", "Dotnond Ltd", null, null },
                    { 4, null, "Guerrilla Games", "7820033114", null },
                    { 5, null, "My Test Company", "7777777777", null },
                    { 6, null, "Test Company Without Address", "6690001315", null }
                });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "City", "CompanyId", "Number", "PostalCode", "Street" },
                values: new object[,]
                {
                    { 1, "Warszawa", 1, "7", "12-123", "Dworcowa" },
                    { 2, "Gdynia", 2, "3", "15-151", "Akustyczna" },
                    { 3, "Poznań", 3, "23", "61-523", "Mielżyńskiego" },
                    { 4, "Poznań", 4, "12", "61-928", "Poznańska" },
                    { 5, "Poznań", 5, "1", "61-458", "Testowa" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CompanyId",
                table: "Addresses",
                column: "CompanyId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Companies");
        }
    }
}
