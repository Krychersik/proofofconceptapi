﻿namespace ProofOfConceptApi.Data.Models
{
    public sealed class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Nip { get; set; }
        public string Krs { get; set; }
        public string Regon { get; set; }

        public Address Address { get; set; }
    }
}