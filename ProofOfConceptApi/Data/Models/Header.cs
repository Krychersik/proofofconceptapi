﻿namespace ProofOfConceptApi.Data.Models
{
    public sealed class Header
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

        public int StatisticId { get; set; }
        public Statistic Statistic { get; set; }
    }
}