﻿using System.Collections.Generic;

namespace ProofOfConceptApi.Data.Models
{
    public sealed class Statistic
    {
        public int Id { get; set; }
        public string RawIdentifier { get; set; }
        public string FormattedIdentifier { get; set; }

        public ICollection<Header> Headers { get; set; }
    }
}