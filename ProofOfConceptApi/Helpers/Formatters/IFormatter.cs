﻿namespace ProofOfConceptApi.Helpers.Formatters
{
    public interface IFormatter
    {
        string Format(string value);
    }
}