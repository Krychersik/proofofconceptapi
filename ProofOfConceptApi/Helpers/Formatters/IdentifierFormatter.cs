﻿using System.Text.RegularExpressions;

namespace ProofOfConceptApi.Helpers.Formatters
{
    public sealed class IdentifierFormatter : IFormatter
    {
        public string Format(string value)
        {
            if (IsValidNipNumberButContainsDashes(value))
            {
                return value.Replace("-", "");
            }

            if (IsValidNipNumberButContainsCountryCode(value))
            {
                return value.Substring(2);
            }

            return value;
        }

        private bool IsValidNipNumberButContainsDashes(string value)
        {
            return new Regex("([0-9]{3}-){2}[0-9]{2}-[0-9]{2}").IsMatch(value);
        }

        private bool IsValidNipNumberButContainsCountryCode(string value)
        {
            return new Regex("[A-Z]{2}[0-9]{10}").IsMatch(value);
        }
    }
}