﻿namespace ProofOfConceptApi.Helpers.Validators
{
    public abstract class AbstractValidator : IValidator
    {
        protected const int Divider = 11;
        protected readonly int[] _multipliers;

        protected AbstractValidator(int[] multipliers)
        {
            _multipliers = multipliers;
        }

        public abstract bool Validate(string value);
        protected abstract bool ContainsExactNumberOfNumericCharacters(string value);

        protected bool ContainsWhiteSpaceCharacters(string value)
        {
            return value.Contains(" ");
        }

        protected int GetSum(string value)
        {
            var sum = 0;

            for (var i = 0; i < value.Length - 1; i++)
            {
                sum += _multipliers[i] * int.Parse(value[i].ToString());
            }

            return sum;
        }
    }
}