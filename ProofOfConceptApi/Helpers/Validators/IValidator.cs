﻿namespace ProofOfConceptApi.Helpers.Validators
{
    public interface IValidator
    {
        bool Validate(string value);
    }
}