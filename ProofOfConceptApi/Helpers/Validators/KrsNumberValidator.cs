﻿using System.Text.RegularExpressions;

namespace ProofOfConceptApi.Helpers.Validators
{
    public sealed class KrsNumberValidator : AbstractValidator
    {
        private const int KrsNumberLength = 10;

        public KrsNumberValidator() : base(new int[] { }) { }

        public override bool Validate(string value)
        {
            if (
                string.IsNullOrEmpty(value) ||
                ContainsWhiteSpaceCharacters(value) ||
                value.Length != KrsNumberLength ||
                !ContainsExactNumberOfNumericCharacters(value)
            )
            {
                return false;
            }

            return true;
        }

        protected override bool ContainsExactNumberOfNumericCharacters(string value)
        {
            return new Regex("[0-9]{10}").IsMatch(value);
        }
    }
}