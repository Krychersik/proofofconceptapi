﻿using System;
using System.Text.RegularExpressions;
using ProofOfConceptApi.Helpers.Formatters;

namespace ProofOfConceptApi.Helpers.Validators
{
    public sealed class NipNumberValidator : AbstractValidator
    {
        private const int NipNumberLength = 10;
        private static readonly int[] Multipliers = {6, 5, 7, 2, 3, 4, 5, 6, 7};

        public NipNumberValidator() : base(Multipliers) { }

        public override bool Validate(string value)
        {
            if (
                string.IsNullOrEmpty(value) ||
                ContainsWhiteSpaceCharacters(value))
            {
                return false;
            }

            var formattedValue = new IdentifierFormatter().Format(value);

            if (!ContainsExactNumberOfNumericCharacters(formattedValue) || formattedValue.Length != NipNumberLength)
            {
                return false;
            }

            try
            {
                var sum = GetSum(formattedValue);
                var checksum = int.Parse(formattedValue[^1].ToString());
                var isValid = sum % Divider == checksum;

                return isValid;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected override bool ContainsExactNumberOfNumericCharacters(string value)
        {
            return new Regex("[0-9]{10}").IsMatch(value);
        }
    }
}