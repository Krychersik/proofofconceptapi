﻿using System;
using System.Text.RegularExpressions;

namespace ProofOfConceptApi.Helpers.Validators
{
    public class RegonNumberValidator : AbstractValidator
    {
        private const int RegonNumberLength = 9;
        private static readonly int[] Multipliers = {8, 9, 2, 3, 4, 5, 6, 7};

        public RegonNumberValidator() : base(Multipliers) { }

        public override bool Validate(string value)
        {
            if (
                string.IsNullOrEmpty(value) ||
                !ContainsExactNumberOfNumericCharacters(value) ||
                ContainsWhiteSpaceCharacters(value) ||
                value.Length != RegonNumberLength)
            {
                return false;
            }

            try
            {
                var sum = GetSum(value);
                var checksum = int.Parse(value[^1].ToString());
                var isValid = sum % Divider == checksum;

                return isValid;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected override bool ContainsExactNumberOfNumericCharacters(string value)
        {
            return new Regex("[0-9]{9}").IsMatch(value);
        }
    }
}