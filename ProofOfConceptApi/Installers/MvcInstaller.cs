﻿using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProofOfConceptApi.Repositories;
using ProofOfConceptApi.Services;
using ProofOfConceptApi.Services.AutoMapper;

namespace ProofOfConceptApi.Installers
{
    public sealed class MvcInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddMvc(options => { options.EnableEndpointRouting = false; })
                .AddNewtonsoftJson()
                .AddFluentValidation(options => { options.RegisterValidatorsFromAssemblyContaining<Startup>(); })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddCors();

            services.AddAutoMapper(typeof(AutoMapperProfiles));

            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddTransient<ICompanyService, CompanyService>();

            services.AddScoped<IStatisticRepository, StatisticRepository>();
            services.AddTransient<IStatisticService, StatisticService>();
        }
    }
}