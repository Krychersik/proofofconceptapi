﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProofOfConceptApi.Data;
using ProofOfConceptApi.Data.Models;

namespace ProofOfConceptApi.Repositories
{
    public sealed class CompanyRepository : ICompanyRepository
    {
        private readonly DataContext _context;

        public CompanyRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Company> GetCompanyByNipNumberAsync(string nipNumber)
        {
            return await _context.Companies
                .Include(c => c.Address)
                .FirstOrDefaultAsync(c => c.Nip == nipNumber);
        }

        public async Task<Company> GetCompanyByRegonNumberAsync(string regonNumber)
        {
            return await _context.Companies
                .Include(c => c.Address)
                .FirstOrDefaultAsync(c => c.Regon == regonNumber);
        }

        public async Task<Company> GetCompanyByKrsNumberAsync(string krsNumber)
        {
            return await _context.Companies
                .Include(c => c.Address)
                .FirstOrDefaultAsync(c => c.Krs == krsNumber);
        }
    }
}