﻿using System.Threading.Tasks;
using ProofOfConceptApi.Data.Models;

namespace ProofOfConceptApi.Repositories
{
    public interface ICompanyRepository
    {
        Task<Company> GetCompanyByNipNumberAsync(string nipNumber);
        Task<Company> GetCompanyByRegonNumberAsync(string regonNumber);
        Task<Company> GetCompanyByKrsNumberAsync(string krsNumber);
    }
}