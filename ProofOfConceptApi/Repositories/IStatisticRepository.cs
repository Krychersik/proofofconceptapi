﻿using System.Threading.Tasks;
using ProofOfConceptApi.Data.Models;

namespace ProofOfConceptApi.Repositories
{
    public interface IStatisticRepository
    {
        void AddStatistic(Statistic statistic);
        Task<bool> SaveAsync();
    }
}