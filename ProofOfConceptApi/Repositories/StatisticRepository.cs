﻿using System.Threading.Tasks;
using ProofOfConceptApi.Data;
using ProofOfConceptApi.Data.Models;

namespace ProofOfConceptApi.Repositories
{
    public sealed class StatisticRepository : IStatisticRepository
    {
        private readonly DataContext _context;

        public StatisticRepository(DataContext context)
        {
            _context = context;
        }

        public void AddStatistic(Statistic statistic)
        {
            _context.Add(statistic);
        }

        public async Task<bool> SaveAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}