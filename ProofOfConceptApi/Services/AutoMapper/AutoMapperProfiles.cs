﻿using AutoMapper;
using ProofOfConceptApi.Contracts.V1.Models;
using ProofOfConceptApi.Data.Models;

namespace ProofOfConceptApi.Services.AutoMapper
{
    public sealed class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Address, AddressDto>();
            CreateMap<Company, CompanyDto>();
        }
    }
}