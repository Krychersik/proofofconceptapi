﻿using System.Threading.Tasks;
using AutoMapper;
using ProofOfConceptApi.Contracts.V1.Models;
using ProofOfConceptApi.Data.Models;
using ProofOfConceptApi.Helpers.Formatters;
using ProofOfConceptApi.Helpers.Validators;
using ProofOfConceptApi.Repositories;

namespace ProofOfConceptApi.Services
{
    public sealed class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _repository;
        private readonly IMapper _mapper;

        public CompanyService(ICompanyRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<CompanyDto> GetCompanyByIdentifierAsync(string identifier)
        {
            Company companyEntity = null;
            var identifierFormatter = new IdentifierFormatter();

            if (new NipNumberValidator().Validate(identifier))
            {
                companyEntity = await _repository.GetCompanyByNipNumberAsync(identifierFormatter.Format(identifier));
            }
            else if (new RegonNumberValidator().Validate(identifier))
            {
                companyEntity = await _repository.GetCompanyByRegonNumberAsync(identifierFormatter.Format(identifier));
            }
            else if (new KrsNumberValidator().Validate(identifier))
            {
                companyEntity = await _repository.GetCompanyByKrsNumberAsync(identifierFormatter.Format(identifier));
            }

            var companyDto = _mapper.Map<CompanyDto>(companyEntity);

            return companyDto;
        }
    }
}