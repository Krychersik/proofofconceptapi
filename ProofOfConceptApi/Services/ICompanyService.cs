﻿using System.Threading.Tasks;
using ProofOfConceptApi.Contracts.V1.Models;

namespace ProofOfConceptApi.Services
{
    public interface ICompanyService
    {
        Task<CompanyDto> GetCompanyByIdentifierAsync(string identifier);
    }
}