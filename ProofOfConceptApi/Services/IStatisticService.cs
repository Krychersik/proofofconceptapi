﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;

namespace ProofOfConceptApi.Services
{
    public interface IStatisticService
    {
        Task AddStatistic(string identifier, ICollection<KeyValuePair<string, StringValues>> headersKeyValuePairs);
    }
}