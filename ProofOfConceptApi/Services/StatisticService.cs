﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using ProofOfConceptApi.Data.Models;
using ProofOfConceptApi.Helpers.Formatters;
using ProofOfConceptApi.Repositories;

namespace ProofOfConceptApi.Services
{
    public sealed class StatisticService : IStatisticService
    {
        private readonly IStatisticRepository _repository;

        public StatisticService(IStatisticRepository repository)
        {
            _repository = repository;
        }

        public async Task AddStatistic(string identifier,
            ICollection<KeyValuePair<string, StringValues>> headersKeyValuePairs)
        {
            if (headersKeyValuePairs == null)
            {
                return;
            }

            var headers = new List<Header>();

            foreach (var (key, value) in headersKeyValuePairs)
            {
                headers.Add(new Header
                {
                    Key = key,
                    Value = value
                });
            }

            var statistic = new Statistic
            {
                RawIdentifier = identifier,
                FormattedIdentifier = new IdentifierFormatter().Format(identifier),
                Headers = headers
            };

            _repository.AddStatistic(statistic);
            await _repository.SaveAsync();
        }
    }
}